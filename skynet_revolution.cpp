#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <list>
#include <iterator>
#include <unordered_set>
#include <cassert>
#include <sstream>

using namespace std;

//region structs

class Graph {
public:
    Graph(int& nodes_count) : nodes_count_(nodes_count),
//        adj_list_(new list<int>[nodes_count]) {}
        adj_list_(nodes_count, list<int>()) {}

    void addEdge(int start, int finish) {
        adj_list_.at(start).push_back(finish);
    }

    const list<int>& adjacent(int p) {
        return adj_list_.at(p);
    }

    void printSelf() {
        ostream_iterator <int> output(cerr, ", ");
        cerr << "{";
        for (int i = 0; i < nodes_count_; ++i) {
            cerr << "\t" << i << " : [";
            std::copy(adj_list_[i].begin(), adj_list_[i].end(), output);
            cerr << "]\n";
        }
        cerr << "}";
    }
private:
    int nodes_count_;
//    list<int>* adj_list_;
    vector<list<int>> adj_list_;
};
struct Node {
    int id;
    int came_from;
};
class Visitor {
public:
    Visitor(int nodes_count) : nodes_count_(nodes_count),
          visited_(new bool[nodes_count]) {
        reset();
    }

    bool isVisited(const Node& node) {
        return visited_[node.id];
    }
    void visit(const Node& node) {
        visited_[node.id] = true;
    }
    void reset() {
        for_each(visited_, visited_ + nodes_count_, [](bool& b) {
            b = false;
        });
        assert(!any_of(visited_, visited_ + nodes_count_,
                       [](bool b) { return b; }));
    }
private:
    int nodes_count_;
    bool* visited_;
};
//endregion

Node getClosestExitEdge(const Graph& graph, const unordered_set<int>& set) {
    return Node();
}
int main()
{
     stringstream cin;
     cin << "3 2 1\n"
           "1 2\n"
           "1 0\n";

    int nodes_count; // the total number of nodes in the level, including the gateways
    int links_count; // the number of links
    int exit_count; // the number of exit gateways
    cin >> nodes_count >> links_count >> exit_count; cin.ignore();
    cerr << nodes_count << " " << links_count << " " << exit_count << endl;

    Graph graph (nodes_count);
    for (int i = 0; i < links_count; i++) {
        int N1; // N1 and N2 defines a link between these nodes
        int N2;
        cin >> N1 >> N2; cin.ignore();
        assert(N1 != N2);
        cerr << N1 << " " << N2 << endl; // " <-> "
        graph.addEdge(N1, N2);
        graph.addEdge(N2, N1);
    }
    graph.printSelf();
    unordered_set<int> exits;
    for (int i = 0; i < exit_count; i++) {
        int EI; // the index of a gateway node
        cin >> EI; cin.ignore();
        exits.insert(EI);
    }

    Visitor visitor(nodes_count);
    // game loop
    while (1) {
        int SI; // The index of the node on which the Skynet agent is positioned this turn
        cin >> SI; cin.ignore();

        Node edge_to_sewer = getClosestExitEdge(graph, exits);
        // Example: 0 1 are the indices of the nodes you wish to sever the link between
        cout << "0 1" << endl;
    }
}
